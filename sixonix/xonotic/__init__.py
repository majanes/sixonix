from .run import run

BENCHMARKS = [
    "low",
    "high",
    "ultra",
    "ultimate",
]
